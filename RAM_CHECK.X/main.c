/*
 * File:   main.c
 * Author: linshaojun
 *
 * Created on November 29, 2021, 11:22 AM
 */



#include <xc.h>

// Configuration bits: selected in the GUI

// CONFIG1
#pragma config FEXTOSC = OFF              // External Oscillator Mode Selection bits->Oscillator not enabled
#pragma config RSTOSC = HFINTOSC_32MHZ    // Power-up Default Value for COSC bits->HFINTOSC (32 MHz)
#pragma config CLKOUTEN = OFF             // Clock Out Enable bit->CLKOUT function is disabled; I/O function on RA4
#pragma config VDDAR = HI                 // VDD Range Analog Calibration Selection bit->Internal analog systems are calibrated for operation between VDD = 2.3V - 5.5V

// CONFIG2
#pragma config MCLRE = EXTMCLR            // Master Clear Enable bit->If LVP = 0, MCLR pin is MCLR; If LVP = 1, RA3 pin function is MCLR
#pragma config PWRTS = PWRT_OFF           // Power-up Timer Selection bits->PWRT is disabled
#pragma config WDTE = ON                  // WDT Operating Mode bits->WDT enabled regardless of Sleep; SEN bit is ignored
#pragma config BOREN = OFF                // Brown-out Reset Enable bits->Brown-out Reset Enabled, SBOREN bit is ignored
#pragma config BORV = LO                  // Brown-out Reset Voltage Selection bit->Brown-out Reset Voltage (VBOR) set to 1.9V
#pragma config PPS1WAY = ON               // PPSLOCKED One-Way Set Enable bit->The PPSLOCKED bit can be set once after an unlocking sequence is executed; once PPSLOCKED is set, all future changes to PPS registers are prevented
#pragma config STVREN = ON                // Stack Overflow/Underflow Reset Enable bit->Stack Overflow or Underflow will cause a reset

// CONFIG4
#pragma config BBSIZE = BB512             // Boot Block Size Selection bits->512 words boot block size
#pragma config BBEN = OFF                 // Boot Block Enable bit->Boot Block is disabled
#pragma config SAFEN = OFF                // SAF Enable bit->SAF is disabled
#pragma config WRTAPP = OFF               // Application Block Write Protection bit->Application Block is not write-protected
#pragma config WRTB = OFF                 // Boot Block Write Protection bit->Boot Block is not write-protected
#pragma config WRTC = OFF                 // Configuration Registers Write Protection bit->Configuration Registers are not write-protected
#pragma config WRTSAF = OFF               // Storage Area Flash (SAF) Write Protection bit->SAF is not write-protected
#pragma config LVP = ON                   // Low Voltage Programming Enable bit->Low Voltage programming enabled. MCLR/Vpp pin function is MCLR. MCLRE Configuration bit is ignored.

// CONFIG5
#pragma config CP = OFF                   // User Program Flash Memory Code Protection bit->User Program Flash Memory code protection is disabled



typedef unsigned char           u8;
typedef signed   char           s8;
typedef unsigned short          u16;
typedef signed   short          s16;
typedef unsigned long           u32;	
typedef signed   long           s32;
typedef float                   f32;
typedef double                  f64;

#define	 BIT0	                0x01
#define	 BIT1	                0x02
#define	 BIT2	                0x04
#define	 BIT3	                0x08
#define	 BIT4	                0x10
#define	 BIT5	                0x20
#define	 BIT6	                0x40
#define	 BIT7	                0x80
#define	 BIT8	                0x0100
#define	 BIT9	                0x0200
#define	 BIT10	                0x0400
#define	 BIT11	                0x0800
#define	 BIT12	                0x1000
#define	 BIT13	                0x2000
#define	 BIT14	                0x4000
#define	 BIT15	                0x8000

#define	pCsH                    RA4 = 1
#define	pCsL                    RA4 = 0
#define	pClkH                   RA5 = 1
#define	pClkL                   RA5 = 0
#define	pDataH                  RA2 = 1
#define	pDataL                  RA2 = 0

#define	pBacklightOn	        RC0 = 1
#define	pBacklightOff	        RC0 = 0


#define CLcdRamLength	        22
#define RAMCHECK_LEN            950	
#define CEepromAddr		0x1FE0

u8 ramcheck[RAMCHECK_LEN];
u8 vLcdRam[CLcdRamLength]={};

void ClrLcdRam()
{
	u8	i;
	for(i = 0; i< CLcdRamLength ; i++)
		{vLcdRam[i] = 0;}
	//display number 8
	vLcdRam[13] |= 0x7f;
	vLcdRam[14] |= 0x7f >> 4;
}

void FullLcdRam()
{
	u8	i;
	for(i = 0; i< CLcdRamLength ; i++)
		{vLcdRam[i] = 0xFF;}
}


void Delay1us(u8 vTime)
{
	u8	vBuffer;
	while(vTime--)
	{	
		vBuffer = 2;
		while(vBuffer)
			{vBuffer--;}
	}
}

void Tm1621IoInit()
{
	pDataH;
	pClkH;
	pCsH;
	Delay1us(3);
	pCsL;
	Delay1us(3);
}
void Tm1621Command(u16	vCommand, u8 vLength)
{
	u8	i;
	for(i = 0; i < vLength; i++)
	{
		pClkL;
		Delay1us(2);
		if(vCommand & BIT15)
			{pDataH;}
		else
			{pDataL;}
		vCommand <<= 1;
		Delay1us(2);
		pClkH;
		Delay1us(3);
	}
	if(vLength == 12)
		{pCsH;}
}
void Tm1621Write()
{
	u8	i,j;

	Tm1621IoInit();
	Tm1621Command(0x9C60, 12);
	Tm1621IoInit();
	Tm1621Command(0x8020, 12);
	Tm1621IoInit();
	Tm1621Command(0x8300, 12);
	Tm1621IoInit();
	Tm1621Command(0x8060, 12);
	Tm1621IoInit();
	Tm1621Command(0x8300, 12);
	Tm1621IoInit();
	Tm1621Command(0x8520, 12);
	//----
	Tm1621IoInit();
	Tm1621Command(0xA000, 9);
	for(i = 0; i < CLcdRamLength; i++)
	{
		for(j = 0; j < 4; j++)
		{
			pClkL;
			Delay1us(5);
			if(vLcdRam[i] & BIT0)
				{pDataH;}
			else
				{pDataL;}
			vLcdRam[i] >>= 1;
			Delay1us(5);
			pClkH;
			Delay1us(10);
		}
	}
}


void ram_check_init()
{
	for(uint16_t i = 0; i < RAMCHECK_LEN; i++)
	{
		ramcheck[i] = (uint8_t)i;
	}
}

//return 1 if fail
u8 ram_check()
{
	for(uint16_t i = 0; i < RAMCHECK_LEN; i++)
	{
		if(ramcheck[i] != (uint8_t)i)
		{
			return 1;
		}
	}

	return 0;
}

const uint8_t EepromTab[32]__at(CEepromAddr)=
{
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
};

u8 flash_read()
{
    uint8_t GIEBitValue = INTCONbits.GIE;    // Save interrupt enable
    
    INTCONbits.GIE = 0;                      // Disable interrupts
    NVMADRL = (CEepromAddr & 0x00FF);
    NVMADRH = ((CEepromAddr & 0xFF00) >> 8);

    NVMCON1bits.NVMREGS = 0;                // Deselect Configuration space
    NVMCON1bits.RD = 1;                     // Initiate Read
    NOP();
    NOP();
    INTCONbits.GIE = GIEBitValue;           // Restore interrupt enable

    return NVMDATL;
}

void flash_write(uint8_t data)
{
    uint8_t GIEBitValue = INTCONbits.GIE;   // Save interrupt enable
    INTCONbits.GIE = 0;                     // Disable interrupts
    NVMADR = CEepromAddr;
    NVMDAT = data;

    NVMCON1bits.NVMREGS = 0;               // Deselect Configuration space
    NVMCON1bits.WREN = 1;                  // Enable wrties

    NVMCON2 = 0x55;
    NVMCON2 = 0xAA;
    NVMCON1bits.WR = 1;

    while (NVMCON1bits.WR);
    NVMCON1bits.WREN = 0;                  // Disable wrties
    INTCONbits.GIE = GIEBitValue;          // Restore interrupt enable
}



void main(void) {
    
    //Clock
    OSCEN   = 0x00;   // MFOEN disabled; LFOEN disabled; ADOEN disabled; HFOEN disabled; 
    OSCFRQ  = 0x03;   // FRQ 8_MHz;
    OSCTUNE = 0x00;   // TUN 0; 

    //IO
    TRISA &= ~(1 << 2);   //RC2 OUT
    TRISA &= ~(1 << 4);   //RC4 OUT 
    TRISA &= ~(1 << 5);   //RC5 OUT
    TRISC &= ~(1 << 0);   //RC0 OUT

    //put index number to corresponding array address.
    ram_check_init();
    
    //Test
    while(1)
    {
        pBacklightOn;
        // if fail, light the whole lcd up.
        if(ram_check() || (flash_read() != 0xff))
        {
            FullLcdRam();
            if(flash_read()== 0xff)
            {
                flash_write(PCON0);
            }
        }
        else
        {
            ClrLcdRam();
        }

        Tm1621Write();
        CLRWDT();
        pBacklightOff;
        Delay1us(200);
        Delay1us(200);
        Delay1us(200);
    }
}
